# Requires Adafruit_Python_PN532

import binascii
import socket
import time
import signal
import sys
import argparse
import Adafruit_PN532 as PN532
import requests
import logging

LOG_FILENAME = 'py_log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.INFO)


class ManageParkEventId:

    def __init__(self):
        self.park_event_id = None

    def get(self):
        return self.park_event_id

    def set_event_id(self, value):
        self.park_event_id = value


def main(args, event_manager):

    # PN532 configuration for a Raspberry Pi GPIO:

    # GPIO 18, pin 12
    CS = int(args.cs)
    # CS = 18
    # GPIO 23, pin 16
    MOSI = int(args.mo)
    # MOSI = 23
    # GPIO 24, pin 18
    MISO = int(args.mi)
    # MISO = 24
    # GPIO 25, pin 22
    SCLK = int(args.sc)
    # SCLK = 25

    # Configure the key to use for writing to the MiFare card.  You probably don't
    # need to change this from the default below unless you know your card has a
    # different key associated with it.
    CARD_KEY = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

    # Number of seconds to delay after reading data.
    DELAY = 0.5

    # Prefix, aka header from the card
    HEADER = b'BG'

    def close(signal, frame):
            sys.exit(0)

    signal.signal(signal.SIGINT, close)

    # Create and initialize an instance of the PN532 class
    pn532 = PN532.PN532(cs=CS, sclk=SCLK, mosi=MOSI, miso=MISO)
    pn532.begin()
    pn532.SAM_configuration()

    print('PN532 NFC RFID 13.56MHz Card Reader')
    while True:
        # Wait for a card to be available
        uid = pn532.read_passive_target()
        # Try again if no card found
        if uid is None:
            event_manager.set_event_id(None)
            continue
        # Found a card, now try to read block 4 to detect the block type
        print('')
        print('Card UID 0x{0}'.format(binascii.hexlify(uid)))
        # Authenticate and read block 4
        if not pn532.mifare_classic_authenticate_block(uid, 4, PN532.MIFARE_CMD_AUTH_B,
                                                       CARD_KEY):
            print('Failed to authenticate with card!')
            event_manager.set_event_id(None)
            continue
        data = pn532.mifare_classic_read_block(4)
        if data is None:
            print('Failed to read data from card!')
            event_manager.set_event_id(None)
            continue
        # Check the header
        if data[0:2] != HEADER:
            print('Card is not written with proper block data!')
            continue
        # Parse out the block type and subtype
        decoded_id = int(data[2:8].decode("utf-8"), 16)
        print('User Id: {0}'.format(decoded_id))
        send_request(decoded_id, args.parkspot, event_manager, args.parkid)
        time.sleep(DELAY)


def send_request(tag_id, parking_spot_id, event_manager, parking_id):
    api_url = 'http://192.168.0.103:8080/parkings/{0}/parkingSpots/{1}/registerEvent'.format(
        parking_id, parking_spot_id)
    data = {'tagId': tag_id}
    if event_manager.get():
        data.update({'parkingEventId': event_manager.get()})

    res = requests.post(api_url, json=data, headers={'Content-Type': 'application/json'})
    if res.status_code == 200:
        logging.info('Request sent successfully for park spot id: {0}, data: {1}'.format(
            parking_spot_id, data))
        event_id = res.json().get('id')
        event_manager.set_event_id(event_id)
    else:
        logging.error('[ERROR] Failed to sent request for tag id: {0} ---> {1}, data -> {2}'.format(
            tag_id, res.content, data))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(prog='PROG', add_help=False)
    requiredNamed = parser.add_argument_group('required named arguments')
    requiredNamed.add_argument('--cs', help='chip select', required=False, default=18)
    requiredNamed.add_argument('--mo', help='master output slave input', required=False, default=23)
    requiredNamed.add_argument('--mi', help='master input slave output', required=False, default=24)
    requiredNamed.add_argument('--sc', help='serial clock', required=False, default=25)
    requiredNamed.add_argument('--parkspot', help='parking spot id', required=False,default=1.0)
    requiredNamed.add_argument('--parkid', help='parking id', required=False, default=1.0)
    arguments = parser.parse_args()
    parking_event_manager = ManageParkEventId()
    main(arguments, parking_event_manager)
